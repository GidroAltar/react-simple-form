import { ActionCreator, Action, ActionCreatorsMapObject } from "redux"
import { Dispatch } from "react-redux";

export const setSaveButtonVisibility: ActionCreator<Action> = (value: boolean) => ({
  type: "SET_SAVE_BUTTON_VISIBILITY",
  payload: {
    value: value
  }
})

export const setSavePictureVisibility: ActionCreator<Action> = (value: boolean) => ({
  type: "SET_SAVE_PICTURE_VISIBILITY",
  payload: {
    value: value
  }
})

export const setErrorMessage: ActionCreator<Action> = (message: string) => ({
  type: "SET_ERROR_MESSAGE",
  payload: {
    message: message
  }
})


export const saveInputChanges = () => {
  return (dispatch: any) => {
    //показываем, что началось сохранение
    dispatch(setSavePictureVisibility(true));
    dispatch(setErrorMessage(""));
    //ждем 2 секунды и скрываем картинку
    let stopPromise = new Promise((resolve: any, reject: any) => {
      setTimeout(() => {
        //эмуляция ошибки с помощью генератора случайных чисел
        if( Math.round(Math.random()) )
          resolve(null);
        else
          reject(new Error("Не удалось сохранить введенное значение!"));
      }, 2000)
    });
    stopPromise
      .then(
            () => { 
              dispatch(setSavePictureVisibility(false));
              dispatch(setSaveButtonVisibility(false)); // скрываем кнопку сохранить, т.к. только что сохранили изменения
             },
            (error: Error) => { 
              dispatch(setSavePictureVisibility(false));
              dispatch(setErrorMessage(error.message)); 
            }
      );
  }
}

export const appActionsMap: ActionCreatorsMapObject = {
  setSaveButtonVisibility: setSaveButtonVisibility,
  setSavePictureVisibility: setSavePictureVisibility,
  setErrorMessage: setErrorMessage,
  saveInputChanges: saveInputChanges
}

export default appActionsMap;