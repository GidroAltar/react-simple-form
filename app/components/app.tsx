import * as React from "react"
import { connect } from "react-redux"
import { bindActionCreators, ActionCreatorsMapObject } from "redux"

import { appActionsMap } from "../actions/index"

interface IState {
    
}

interface IProps {
    appState: any,
    appActions: any
}

class App extends React.Component<IProps, IState> {


    handleInputChange = () => {
        if (this.props.appState.saveButtonVisible)
            return;
        this.props.appActions.setSaveButtonVisibility(true);
    }

    handleSaveButtonClick = () => {
        if (this.props.appState.isInputChangesSaving)
            return;
        this.props.appActions.saveInputChanges();
    }

    render() {    
        const { saveButtonVisible, isInputChangesSaving, errorMessage } = this.props.appState;
        return <div className="container">
            <div className="row">
                <div className="col-sm-12">
                    <form>
                        <label htmlFor="email">Field:</label>
                        <input type="text" className="form-control" id="email" onChange = { this.handleInputChange }></input>
                        {
                            (saveButtonVisible) ?
                            <button type="button" className="btn" onClick = { this.handleSaveButtonClick }>Save</button> :
                            null
                        }
                        {
                            (errorMessage) ?
                            <div>{ errorMessage }</div>:
                            null
                        }
                        {
                            (isInputChangesSaving) ?
                            <div><img src = "https://loading.io/spinners/spinner/lg.ajax-spinner-preloader.gif"></img></div>:
                            null
                        }
                    </form>
                </div>
            </div>
        </div>
    }
}

function mapStateToProps (state: any) {
  return {
    appState: state
  }
}

function mapDispatchToProps(dispatch: any) {  
  return {
    appActions: bindActionCreators(appActionsMap, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);