
const mainReducer = (state: any, action: any) => {
  switch (action.type) {
    case "SET_SAVE_BUTTON_VISIBILITY":
        return { ...state, saveButtonVisible: action.payload.value };
    case "SET_SAVE_PICTURE_VISIBILITY":
        return { ...state, isInputChangesSaving: action.payload.value };
    case "SET_ERROR_MESSAGE":
        return { ...state, errorMessage: action.payload.message }
    default:
      return state
  }
}

export default mainReducer